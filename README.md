# Bug report doctrine-attribute-index-bug

## versions

- PHP 8.1
- MySQL 5.7
- Symfony 5.4.12
- Doctrin ORM 3.12.1

(optional)
- Skipper ORM Designer 3.3.2.1792

## problem

this way of defining indexes seem not to be working

```php
#[ORM\Table(
    name: 'custom_article_table_name',
    indexes: [
        new ORM\Index(name: 'name_index', columns: ['name'])
    ]
)]
```

## steps to reproduce

1. clone this repository
2. configure a database in `.env.local` by setting `DATABASE_URL="mysql://..."`
3. run command `$ php bin/console d:d:c` to create the database
3. run command `$ php bin/console d:s:u --dump-sql` to see the SQL querry Doctrine generated

you WILL see this result:

```mysql
CREATE TABLE custom_invoice_table_name [...] INDEX date_index (date) [...] ;
CREATE TABLE custom_article_table_name [...] ;
```
 
while you SOULD see :

```mysql
CREATE TABLE custom_invoice_table_name [...] INDEX date_index (date) [...] ;
CREATE TABLE custom_article_table_name [...] INDEX name_index (name) [...] ;
```


### workarround 1: do not use the indexes: parameter of ORM\Table

Use `#[ORM\Index]` attribute instead

```php
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
#[ORM\Table(name: 'custom_invoice_table_name')]
#[ORM\Index(name: 'date_index', columns: ['date'])]
class Invoice 
{...}
```

### workarround 2: use annotation

See branch using-annotation-ok of current repo (https://gitlab.com/idlab-public/doctrine-attribute-index-bug/-/tree/using-annotation-ok)

When using annotation, the indexes work fine and the indexed MUST be defined in an array inside the `@ORM\Table` annotation as `#[ORM\Index]` ONLY works with PHP attributes.

Error when trying to use `@ORM\Index` annotation :

`Annotation @ORM\Index is not allowed to be declared on class App\Entity\Invoice. You may only use this annotation on these code elements: ANNOTATION`


related articles

- https://stackoverflow.com/questions/73038623/why-doctrine-migrations-ignore-my-index-declaration
- https://stackoverflow.com/questions/72744338/doctrine-migration-created-by-symfonymakerbundle-tries-to-remove-existing-indexe